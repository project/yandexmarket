<?php
/**
 * @file
 * Contains the yandexmarket default style plugin.
 */

/**
 * Implementation of hook_views_default_views().
 */
function yandexmarket_views_default_views() {
  /*
   * View 'yandexmarket'
   */
  $view = new view;
  $view->name = 'yandexmarket';
  $view->description = '';
  $view->tag = '';
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $fields = array(
    'nid' => array(
      'label' => 'Nid',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_node' => 0,
      'exclude' => 0,
      'id' => 'nid',
      'table' => 'node',
      'field' => 'nid',
      'override' => array(
        'button' => 'Override',
      ),
      'relationship' => 'none',
    ),
    'title_1' => array(
      'label' => 'Название',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_node' => 0,
      'exclude' => 0,
      'id' => 'title_1',
      'table' => 'node',
      'field' => 'title',
      'override' => array(
        'button' => 'Override',
      ),
      'relationship' => 'none',
    ),
    'node_url' => array(
      'label' => 'Node url',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'exclude' => 0,
      'id' => 'node_url',
      'table' => 'node',
      'field' => 'node_url',
      'override' => array(
        'button' => 'Override',
      ),
      'relationship' => 'none',
    ),
    'teaser' => array(
      'label' => 'Краткая аннотация',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'exclude' => 0,
      'id' => 'teaser',
      'table' => 'node_revisions',
      'field' => 'teaser',
      'override' => array(
        'button' => 'Override',
      ),
      'relationship' => 'none',
    ),
  );
  if (module_exists('uc_product')) {
    $fields += array(
      'yandexmarket_field_image_cache_fid' => array(
        'label' => 'Image',
        'alter' => array(
          'alter_text' => 0,
          'text' => '',
          'make_link' => 0,
          'path' => '',
          'link_class' => '',
          'alt' => '',
          'prefix' => '',
          'suffix' => '',
          'target' => '',
          'help' => '',
          'trim' => 0,
          'max_length' => '',
          'word_boundary' => 1,
          'ellipsis' => 1,
          'strip_tags' => 0,
          'html' => 0,
        ),
        'empty' => '',
        'hide_empty' => 0,
        'empty_zero' => 0,
        'link_to_node' => 0,
        'label_type' => 'widget',
        'format' => 'url_plain',
        'multiple' => array(
          'group' => TRUE,
          'multiple_number' => '',
          'multiple_from' => '',
          'multiple_reversed' => FALSE,
        ),
        'exclude' => 0,
        'id' => 'yandexmarket_field_image_cache_fid',
        'table' => 'node_data_field_image_cache',
        'field' => 'yandexmarket_field_image_cache_fid',
        'relationship' => 'none',
      ),
      'sell_price' => array(
        'label' => 'Sell price',
        'alter' => array(
          'alter_text' => 0,
          'text' => '',
          'make_link' => 0,
          'path' => '',
          'link_class' => '',
          'alt' => '',
          'prefix' => '',
          'suffix' => '',
          'target' => '',
          'help' => '',
          'trim' => 0,
          'max_length' => '',
          'word_boundary' => 1,
          'ellipsis' => 1,
          'strip_tags' => 0,
          'html' => 0,
        ),
        'empty' => '',
        'hide_empty' => 0,
        'empty_zero' => 0,
        'set_precision' => 0,
        'precision' => '0',
        'decimal' => '.',
        'separator' => ',',
        'prefix' => '',
        'suffix' => '',
        'format' => 'numeric',
        'exclude' => 0,
        'id' => 'sell_price',
        'table' => 'uc_products',
        'field' => 'sell_price',
        'override' => array(
          'button' => 'Override',
        ),
        'relationship' => 'none',
      ),

    'yandexmarket_tid' => array(
      'label' => 'One term id',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'type' => 'separator',
      'separator' => ', ',
      'link_to_taxonomy' => 0,
      'limit' => 1,
      'vids' => array(
        '1' => 1,
      ),
      'exclude' => 0,
      'id' => 'yandexmarket_tid',
      'table' => 'term_node',
      'field' => 'yandexmarket_tid',
      'relationship' => 'none',
    ),
    );
  }
  $handler->override_option('fields', $fields);
  $handler->override_option('filters', array(
    'status' => array(
      'operator' => '=',
      'value' => '1',
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'status',
      'table' => 'node',
      'field' => 'status',
      'override' => array(
        'button' => 'Override',
      ),
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('items_per_page', 0);
  $handler->override_option('distinct', 1);
  $handler->override_option('style_plugin', 'yandexmarket');
  global $base_url, $base_path;
  $options = array(
    'shop' => array(
      'name' => variable_get('uc_store_name', variable_get('site_name', '')),
      'company' => variable_get('uc_field_company', ''),
      'url' => $base_url . $base_path,
    ),
    'currencies' => '<currency id="RUR" rate="1" />',
    'vocabulary' => variable_get('uc_catalog_vid', ''),
    'yacolumns' => array(
      'id' => array(
        'field' => '',
        'static' => '',
      ),
      'bid' => array(
        'field' => '',
        'static' => '',
      ),
      'cbid' => array(
        'field' => '',
        'static' => '',
      ),
      'available' => array(
        'field' => '',
        'static' => '',
      ),
      'type' => array(
        'field' => '--static--',
        'static' => 'vendor.model',
      ),
      'url' => array(
        'field' => 'node_url',
        'static' => '',
      ),
      'price' => array(
        'field' => '',
        'static' => '',
      ),
      'local_delivery_cost' => array(
        'field' => '',
        'static' => '',
      ),
      'currencyId' => array(
        'field' => '--static--',
        'static' => 'RUR',
      ),
      'categoryId' => array(
        'field' => '',
        'static' => '',
      ),
      'picture' => array(
        'field' => 'yandexmarket_field_image_cache_fid',
        'static' => '',
      ),
      'typePrefix' => array(
        'field' => '',
        'static' => '',
      ),
      'vendor' => array(
        'field' => '',
        'static' => '',
      ),
      'model' => array(
        'field' => '',
        'static' => '',
      ),
      'name' => array(
        'field' => 'title_1',
        'static' => '',
      ),
      'delivery' => array(
        'field' => '',
        'static' => '',
      ),
      'description' => array(
        'field' => 'teaser',
        'static' => '',
      ),
      'vendorCode' => array(
        'field' => '',
        'static' => '',
      ),
      'sales_notes' => array(
        'field' => '',
        'static' => '',
      ),
      'manufacturer_warranty' => array(
        'field' => '',
        'static' => '',
      ),
      'country_of_origin' => array(
        'field' => '',
        'static' => '',
      ),
      'downloadable' => array(
        'field' => '',
        'static' => '',
      ),
      'author' => array(
        'field' => '',
        'static' => '',
      ),
      'publisher' => array(
        'field' => '',
        'static' => '',
      ),
      'series' => array(
        'field' => '',
        'static' => '',
      ),
      'year' => array(
        'field' => '',
        'static' => '',
      ),
      'ISBN' => array(
        'field' => '',
        'static' => '',
      ),
      'volume' => array(
        'field' => '',
        'static' => '',
      ),
      'part' => array(
        'field' => '',
        'static' => '',
      ),
      'language' => array(
        'field' => '',
        'static' => '',
      ),
      'binding' => array(
        'field' => '',
        'static' => '',
      ),
      'page_extent' => array(
        'field' => '',
        'static' => '',
      ),
      'table_of_contents' => array(
        'field' => '',
        'static' => '',
      ),
    ),
  );
  if (module_exists('uc_product')) {
    $options['yacolumns']['price']['field'] = 'sell_price';
    $options['yacolumns']['categoryId']['field'] = 'yandexmarket_tid';
  }
  $handler->override_option('style_options', $options);
  $handler = $view->new_display('pagenolayout', 'Page with no layout', 'pagenolayout_1');
  $handler->override_option('path', 'yandexmarket');
  $handler->override_option('menu', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
  ));
  $views[$view->name] = $view;

  return $views;
}


